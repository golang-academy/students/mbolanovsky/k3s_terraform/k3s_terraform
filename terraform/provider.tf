terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.53.0"
    }
  }
}

/*
provider "aws" {
region     = "us-east-1"
access_key = var.TF_VAR_aws_access_key
secret_key = var.TF_VAR_aws_sec_key
}
*/
