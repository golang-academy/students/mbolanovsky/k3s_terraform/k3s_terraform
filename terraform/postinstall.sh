#!/bin/bash

# Create User
useradd -s /bin/bash -c "Admin" -m tux
echo "Passw0rd" | passwd --stdin tux
# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMRDX8m5fb/2Ri6NhXeWVE8sWyJRuMZ8+JCQ1b+m4YnH eddsa-key-20230208
EOF
# Set proper permissions
chown -R tux:tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##
